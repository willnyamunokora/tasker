import firebase from 'firebase';

const firebaseApp = firebase.initializeApp({
  apiKey: 'AIzaSyA4G9zjPs-DXONIczIBT3Mzj_DfrtDr0jo',
  authDomain: 'fir-react-project-18a25.firebaseapp.com',
  projectId: 'fir-react-project-18a25',
  storageBucket: 'fir-react-project-18a25.appspot.com',
  messagingSenderId: '122816712688',
  appId: '1:122816712688:web:9fd1910e3cf0ad0c175735',
  measurementId: 'G-8TDE93QP0W',
});

const db = firebaseApp.firestore();

export default db;
