import PropTypes from 'prop-types';

const Button = ({ text, myClick }) => {
  return (
    <div>
      <button type="button" onClick={myClick}>
        {text}
      </button>
    </div>
  );
};

Button.propTypes = {
  text: PropTypes.string,
  myClick: PropTypes.func,
};

export default Button;
