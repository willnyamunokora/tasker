import { FaTimes } from 'react-icons/fa';
import Task from './Task';
import db from '../firebase';

const Tasks = ({ tasks }) => {
  return (
    <div>
      {tasks.map((task) => {
        return (
          <>
            <Task title={task.task} key={task.id} />
            <FaTimes
              onClick={() => db.collection('tasks').doc(task.id).delete()}
            />
          </>
        );
      })}
    </div>
  );
};

export default Tasks;
