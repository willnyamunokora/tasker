import React, { useState } from 'react';

const AddForm = ({ onAdd }) => {
  const [input, setInput] = useState('');

  const onSubmit = (e) => {
    e.preventDefault();
    if (!input) {
      alert('Please enter input');
    }
    onAdd({ input });
    setInput('');
    // console.log(input);
  };
  return (
    <form onSubmit={onSubmit}>
      <input
        type="text"
        value={input}
        placeholder="Input"
        onChange={(e) => setInput(e.target.value)}
      />
      <button type="submit">Add</button>
    </form>
  );
};

export default AddForm;
