import React, { useState, useEffect } from 'react';
import './App.css';
import Tasks from './components/Tasks';
import AddForm from './components/AddForm';
import db from './firebase';

function App() {
  const [tasks, setTasks] = useState([]);

  useEffect(() => {
    db.collection('tasks').onSnapshot((snapshot) => {
      console.log(snapshot.docs.map((doc) => doc.data().task));
      console.log(snapshot.docs.map((doc) => doc.id));
      setTasks(
        snapshot.docs.map((doc) => ({ id: doc.id, task: doc.data().task })),
      );
    });
  }, []);

  const addTask = (task) => {
    console.log(task.input);
    db.collection('tasks').add({
      task: task.input,
    });
  };

  return (
    <div className="App">
      <header className="App-header">
        <h1>Daily Task</h1>
        <AddForm onAdd={addTask} />
        {tasks.length > 0 ? <Tasks tasks={tasks} /> : 'No Tasks Whatsoever'}
      </header>
    </div>
  );
}

export default App;
